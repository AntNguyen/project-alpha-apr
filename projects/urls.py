from django.urls import path
from projects.views import project_view, project_detail_view, create_project

urlpatterns = [
    path("", project_view, name="list_projects"),
    path("create/", create_project, name="create_project"),
    path("<int:id>/", project_detail_view, name="show_project"),
]
