from django.urls import path
from tasks.views import create_task, my_task_view


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", my_task_view, name="show_my_tasks"),
]
