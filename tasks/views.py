from django.shortcuts import render, redirect
from tasks.forms import CreateTask
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(redirect_field_name="login")
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTask()
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required(redirect_field_name="login")
def my_task_view(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/my_tasks.html", context)
